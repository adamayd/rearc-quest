# rearc-quest

## Goals

- [x] 1. If you know how to use git, start a git repository (local-only is acceptable) and commit all of your work to it.
- [ ] 2. Deploy the app in any public cloud and navigate to the index page. Use Linux 64-bit x86/64 as your OS (Amazon Linux preferred in AWS, Similar Linux flavor preferred in GCP and Azure)
- [ ] 3. Deploy the app in a Docker container. Use `node` as the base image. Version `node:10` or later should work.
- [ ] 4. Inject an environment variable (`SECRET_WORD`) in the Docker container. The value of `SECRET_WORD` should be the secret word discovered on the index page of the application.
- [ ] 5. Deploy a load balancer in front of the app.
- [x] 6. Use Infrastructure as Code (IaC) to "codify" your deployment. Terraform is ideal, but use whatever you know, e.g. CloudFormation, CDK, Deployment Manager, etc.
- [ ] 7. Add TLS (https). You may use locally-generated certs.

## Retro

### What went well

- I had a blast going through the quest (for the most part).
- I learned about changes from v0.12 to v1 of Terraform.
- I was able to update my freshly rebuilt laptop with all of the cloud tooling that I hadn't installed yet.

### What didn't go well

- Time management was poor.  I looked at the quest Thursday evening to make sure I had answers to questions before the weekend. I didn't have the time over the weekend and started working on this Monday night.  In total I spent between 5-6 hours.  I was close to completion, but still far enough away to realize the TODOs were greater than the time left.  
- The startup script in `user_data` was sporadic at best and not the best option.
- I had an issue with local running of the docker container on `terraform apply` that took a bit of time to debug.
- I didn't complete in any of the cloud providers when initially I was hoping to at least complete in AWS and GCP.
- I tried too simple of approaches with the thought of building out as I went instead of more familiar standard practices...
- Which also led to poor file management in the repo.  I should have used standards or boilerplate but I honestly thought the tf files would be rather small and I could name each by provider.

### Actions

- Complete the quest in all three providers.
- Continue to go down this original simplified path to completion.
- Complete with further built out plans (see [Given more time I would...](#Given more time I would...)).
- Terraform training to pick up the latest differences (like `aws_subnet_ids` vs filtering `aws_subnets`).

## Given more time I would...

Plan better for a complete picture and use methods I'm more familiar with in practice.  Instead of trying to run EC2 instances direct with docker images, using ECS/EKS and ECR for Docker image.  Either EC2 or Fargate with ASGs.  Proper key store or vault for the secret word.  AMI creation with ansible if continuing with the EC2 direct path.  Builds in CI/CD pipeline.  Proper boilerplate files for TF (data, resources, variables, etc). Possibly cross product (ie. Github, Circle CI, AWS) and single product (CodeCommit, CodePipeline, ECS in AWS) options.  

## Final thoughts

This was a failure in terms of an tech challenge interview, but a success in a personal career moment.  Knowing that I failed the quest is enough to motivate me to re-focus my studies.  I was once strong in these areas, and lack of use has really shown through.  I appreciate the opportunity to take the quest in the clouds.
