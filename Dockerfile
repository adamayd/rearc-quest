FROM node:16

RUN git clone https://github.com/rearc/quest.git /app

WORKDIR /app
RUN npm install

ENTRYPOINT ["node"]
CMD ["src/000.js"]
