# Set up
provider "aws" {
  region = "us-east-2"
}

terraform {
  required_version = ">1.1"
}

resource "aws_budgets_budget" "rearc-quest" {
  name              = "rearc-monthly"
  budget_type       = "COST"
  limit_amount      = "20.00"
  limit_unit        = "USD"
  time_unit         = "MONTHLY"
  time_period_start = "2022-04-04_00:01"
}

# Sources

variable "ssh_ip" {
  description = "Local IP address for SSH access to EC2 instances."
  type        = string
  sensitive   = true
}

variable "secret_word" {
  description = "Secret word to pass to the Environment Variables"
  type        = string
  sensitive   = true
}

variable "docker_port" {
  description = "The port of the docker container for ALB to hit application."
  type        = number
  default     = 3000
}

output "alb_dns_name" {
  value       = aws_lb.application_lb.dns_name
  description = "The domain name of the load balancer"
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }
}

data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}

# VPC - Default

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

# SGs

resource "aws_security_group" "application_lb_sg" {
  name = "application_lb_sg"

  # Allow inbound requests from all traffic on port 80
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound requests
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "application_server_sg" {
  name = "application_server_sg"

  # Allow inbound requests on docker_port
  ingress {
    from_port   = var.docker_port
    to_port     = var.docker_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow for SSH access
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_ip}/32"]
  }

  # Allow all outbound requests
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Load Balancer

resource "aws_lb" "application_lb" {
  name               = "application-lb"
  internal           = false
  load_balancer_type = "application"
  subnets            = data.aws_subnets.default.ids
  security_groups    = [aws_security_group.application_lb_sg.id]
}

resource "aws_lb_target_group" "alb-proxy-tg" {
  name     = "alb-proxy-target-group"
  port     = 3000
  protocol = "HTTP"
  vpc_id   = aws_default_vpc.default.id
}

resource "aws_lb_target_group_attachment" "ec2_target" {
  target_group_arn = aws_lb_target_group.alb-proxy-tg.arn
  target_id        = aws_instance.docker_application.id
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.application_lb.arn
  port              = 80

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb-proxy-tg.arn
  }
}

# EC2 Instances
resource "aws_instance" "docker_application" {
  ami                    = data.aws_ami.amazon_linux_2.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.application_server_sg.id]
  key_name               = "rearc-quest"
  user_data              = <<-EOF
    #!/bin/bash
    export SECRET_WORD=${var.secret_word}
    sudo yum update -y
    sudo amazon-linux-extras install docker -y
    sudo systemctl start docker
    sudo curl https://gitlab.com/adamayd/rearc-quest/-/raw/main/Dockerfile -O
    sudo docker build -t rearc-quest .
    sudo docker run --rm -it -p 3000:3000 -e SECRET_WORD=$SECRET_WORD rearc-quest
  EOF

}
